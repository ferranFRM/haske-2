-------------------------------------------------------------------------------------
----------------------------- PRÀCTICA DE HASKELL 2019 ------------------------------
-------------------------------------------------------------------------------------
-------------------------- Ferran Rodríguez, Miguel Ángel ---------------------------

import Data.Char
import Data.List

------------------------------ Definició de CONSTANTS --------------------------------
--------------------------------------------------------------------------------------

taulerInicial = Tauler [(TN,Posicio('a',8)),(CN,Posicio('b',8)),(AN,Posicio('c',8)),(DN,Posicio('d',8)),(RN,Posicio('e',8)),(AN,Posicio('f',8)),(CN,Posicio('g',8)),(TN,Posicio('h',8)),
                        (PN,Posicio('a',7)),(PN,Posicio('b',7)),(PN,Posicio('c',7)),(PN,Posicio('d',7)),(PN,Posicio('e',7)),(PN,Posicio('f',7)),(PN,Posicio('g',7)),(PN,Posicio('h',7)),
                        (PB,Posicio('a',2)),(PB,Posicio('b',2)),(PB,Posicio('c',2)),(PB,Posicio('d',2)),(PB,Posicio('e',2)),(PB,Posicio('f',2)),(PB,Posicio('g',2)),(PB,Posicio('h',2)),
                        (TB,Posicio('a',1)),(CB,Posicio('b',1)),(AB,Posicio('c',1)),(DB,Posicio('d',1)),(RB,Posicio('e',1)),(AB,Posicio('f',1)),(CB,Posicio('g',1)),(TB,Posicio('h',1))]


type Partida = (Tauler, Color)
type Jugada = (Peca, Posicio,Posicio)

data Posicio = Posicio (Char, Int) deriving (Eq)
data Peca = PN | PB | CN | CB | AN | AB | TN | TB | DN | DB | RN | RB deriving (Eq)
data Tauler = Tauler [(Peca, Posicio)]
data Color = Blanc | Negre deriving (Show,Eq)

----------------------------------- INSTANCIACIONS -----------------------------------
--------------------------------------------------------------------------------------

instance Show Peca where
 show PN = "p"
 show PB = "P"
 show CN = "c"
 show CB = "C"
 show AN = "a"
 show AB = "A"
 show TN = "t"
 show TB = "T"
 show DN = "d"
 show DB = "D"
 show RN = "r"
 show RB = "R"
 
instance Show Posicio where
 show (Posicio (a,b)) = "(" ++ show a++ "," ++ show b ++ ")"

-- instance Ord Posicio where
--  (Posicio(a,b)) `compare` (Posicio(c,d)) = a `compare` c && b `compare` d
 
instance Show Tauler where
 show (Tauler t) = "   ==========\n" ++ mostrarPeces (Tauler t) ++ "   ==========\n"

instance Ord Posicio where
 compare (Posicio(a,b)) (Posicio(c,d))
  | a == c && b == d = EQ
  | a <= c && b >= d = LT
  | otherwise = GT
--  Posicio(a,b) <= Posicio(c,d) = True
--  Suc x <= Suc y = x <= y
--  <= = False

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------
successorPosicio :: Posicio -> Posicio
successorPosicio (Posicio (a,b))
 | a=='h' && b==1 = error("Successor posicio erroni")
 | a=='h' && b>1 = Posicio('a',(b-1))
 | a<'h' = Posicio((a!+1),b)

posicioLimit :: Posicio
posicioLimit = Posicio('h',1)

iMostrarPeces :: Posicio -> Tauler -> String
iMostrarPeces (Posicio(a,b)) (Tauler t)
  | (Posicio(a,b)) == posicioLimit && (Posicio(a,b)) == snd x = show (fst x) ++ "|\n"             --es ultima posicio i esta al tauler
  | (Posicio(a,b)) == posicioLimit = show b ++ "|\n"                                              --es ultima posicio final
  | ((Posicio(a,b)) == snd x) && (a == 'a') = show b ++ "- |" ++ show (fst x) ++ iMostrarPeces (successorPosicio (Posicio(a,b))) (Tauler(tail t))  --es posicio inicial columna i esta al tauler
  | (Posicio(a,b)) == snd x && a == 'h' = show (fst x) ++ "|\n" ++ iMostrarPeces (successorPosicio (Posicio(a,b))) (Tauler(tail t))                --es posicio final columna i esta al tauler
  | a == 'a' = show b ++ "- |." ++ iMostrarPeces (successorPosicio (Posicio(a,b))) (Tauler t)
  | a == 'h' = ".|\n" ++ iMostrarPeces (successorPosicio (Posicio(a,b))) (Tauler t)
  | (Posicio(a,b)) == snd x = show (fst x) ++ iMostrarPeces (successorPosicio (Posicio(a,b))) (Tauler(tail t))
  | otherwise = "." ++ iMostrarPeces (successorPosicio (Posicio(a,b))) (Tauler t)
  where
  x = head t

mostrarPeces :: Tauler -> String
mostrarPeces (Tauler t) = iMostrarPeces (Posicio ('a',8)) (Tauler t)


--------------------------------------------------
--------------------MOVIMENTS---------------------
--------------------------------------------------

{- Retorna tots els moviments del rei possible a través de la Posicio entrada.
   Un Rei pot fer un salt en qualsevol direcció, sense sortir-se del tauler -}
   -- EX: movimentRei (Posicio ('a',2))
movimentRei :: Posicio -> [Posicio]
movimentRei (Posicio (g,h)) = (vt g h 1) ++ (vt g h (-1)) ++ (vt (g!+1) h 0) ++ (vt (g!+(-1)) h 0) ++ (vt (g!+1) h 1) ++ (vt (g!+1) h (-1)) ++ (vt (g!+(-1)) h (-1)) ++ (vt (g!+(-1)) h 1)
  where 
      vt g h x = if (dinsDelsMarges g (h+x)) then [Posicio (g,h+x)] else []

{- Retorna tots els moviments del Cavall possible a través de la Posicio entrada.
   Un Cavall fa salts en forma de L i pot tenir fins a un total de 8 possibilitats -}
   -- EX: movimentCavall (Posicio ('a',2))
movimentCavall :: Posicio -> [Posicio]
movimentCavall (Posicio (g,h)) = (vld g (h+1) (-2)) ++ (vld g (h+1) 2) ++ (vld g (h-1) (-2)) ++ (vld g (h-1) 2) ++ (vld g (h+2)(-1)) ++ (vld g (h+2) 1) ++ (vld g (h-2) (-1)) ++ (vld g (h-2) 1)
  where
     vld g h a = if (dinsDelsMarges (g!+a) h) then [Posicio (g!+a,h)] else []

{- Retorna tots els moviments de l'alfil possible a través de la Posicio entrada.
   Un Alfil fa salts en diagonal i pot tenir diferent número de possibilitats depenent de la posició on es trobi -}
   -- EX: movimentAlfil (Posicio ('c',3))
movimentAlfil :: Posicio -> [Posicio]
movimentAlfil (Posicio (g,h))= (diagEsAd g (h+1) (-1)) ++ (diagEsAb g (h-1) (-1)) ++ (diagDrAd g (h+1) 1) ++ (diagDrAb g (h-1) 1)

{- Retorna tots els moviments de la torre possible a través de la Posicio entrada.
   Una torre fa salts en horitzontal i pot tenir diferent número de possibilitats depenent de la posició on es trobi -}
   -- EX: movimentTorre (Posicio ('d',2))
movimentTorre :: Posicio -> [Posicio]
movimentTorre (Posicio (g,h)) = (hrz g h 1) ++ (hrz g h (-1)) ++ (vrt g h 1) ++ (vrt g h (-1))

{- Retorna tots els moviments del peo possible a través de la Posicio entrada.
   Un Peó pot fer 1 o 2 salts en la primera jugada i només 1 salt en la resta.
   Com que no es sap qui és el jugador que està efectuant el moviment es considerara ...  -}
   -- EX: movimentPeo (Posicio ('d',2)) Negre, movimentPeo (Posicio ('d',2)) Blanc
movimentPeo :: Posicio -> Color -> [Posicio]
movimentPeo (Posicio (g,h)) c = if (c==Blanc) then (if (h==2) then ((vt g h 1) ++ (vt g h 2)) else (vt g h 1)) else (if (h==7) then ((vt g h (-1)) ++ (vt g h (-2))) else (vt g h (-1)))
  where 
      vt g h x = if (dinsDelsMarges g (h+x)) then [Posicio (g,h+x)] else []



--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

--------------------------------------------------
--------------------AUXILIARS---------------------
--------------------------------------------------
{- Retorna el color de la Peca. Precondició: La peca ha d'existir i no pot ser B -}
colorPeca :: Peca -> Color
colorPeca x = if (x==PB || x==CB || x==AB || x==TB || x==DB || x==RB) then Blanc else Negre

{- Retorna tots els moviments en diagonal cap a l'esquerra i cap adalt a partir de la posició donada i el numero de salts de columna 'a' -}
diagEsAd :: Char -> Int -> Int -> [Posicio]
diagEsAd g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (diagEsAd (g!+a) (h+1) (-1))) else []

{- Retorna tots els moviments en diagonal cap a l'esquerra i cap abaix a partir de la posició donada i el numero de salts de columna 'a' -}
diagEsAb :: Char -> Int -> Int -> [Posicio]
diagEsAb g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (diagEsAb (g!+a) (h-1) (-1))) else []

{- Retorna tots els moviments en diagonal cap a la dreta i cap adalt a partir de la posició donada i el numero de salts de columna 'a' -}
diagDrAd :: Char -> Int -> Int -> [Posicio]
diagDrAd g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (diagDrAd (g!+a) (h+1) 1)) else []

{- Retorna tots els moviments en diagonal cap a la dreta i cap abaix a partir de la posició donada i el numero de salts de columna 'a' -}
diagDrAb :: Char -> Int -> Int -> [Posicio]
diagDrAb g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (diagDrAb (g!+a) (h-1) 1)) else []

{- Retorna tots els moviments en horitzontal a partir de la posició donada, la direcció depén del valor de la columna 'a' -}
hrz :: Char -> Int -> Int -> [Posicio]
hrz g h a = if (dinsDelsMarges (g!+a) h) then ([Posicio (g!+a,h)] ++ (hrz (g!+a) h a)) else []

{- Retorna tots els moviments en vertical a partir de la posició donada, la direcció depén del valor de la fila 'a' -}
vrt :: Char -> Int -> Int -> [Posicio]
vrt g h a = if (dinsDelsMarges g (h+a)) then ([Posicio (g,h+a)] ++ (vrt g (h+a) a)) else []


{- La funció dinsDelsMarges retorna cert si la posició es dins dels marges del Tauler, és a dir, files de 1 a 8 i columnes de 'a' a 'h', altrament fals-}
dinsDelsMarges :: Char -> Int -> Bool
dinsDelsMarges x y
  | y<1 || y>8 = False
  | x>'h' || x<'a' = False
  | otherwise = True
  
{- !+ és un operador que retorna la lletra que ve despres de n salts (Int) de la lletra que hem introduit (Char), si no pertany a cap columna retorna 'z' -}
(!+) :: Char -> Int -> Char
ch !+ a = if ((vv a)<'i' && (vv a)>'`') then vv a else 'z'
  where
     vv a = (chr(ord ch + a))


--- ALGU ENTRE
{- La funció alguEntre retorna cert si hi ha algú entre les posicions donades, sense contemplar aquestes dues. -}
alguEntre :: Tauler -> Posicio -> Posicio -> Bool
alguEntre t (Posicio (a,b)) (Posicio (x,y))
  | a == x && b == y = False
  | a == x && b < y = alguPosicions t (init (tail (vertical (Posicio (x,y)) (Posicio (a,b)))))
  | a == x && b > y = alguPosicions t (init (tail (vertical (Posicio (a,b)) (Posicio (x,y)))))
  | a > x && b == y = alguPosicions t (init (tail (horitzontal (Posicio (a,b)) (Posicio (x,y)))))
  | a < x && b == y = alguPosicions t (init (tail (horitzontal (Posicio (x,y)) (Posicio (a,b)))))
  | a < x && b < y = alguPosicions t (init (tail (diagonalBA (Posicio (a,b)) (Posicio (x,y)))))
  | a < x && b > y = alguPosicions t (init (tail (diagonalAB (Posicio (x,y)) (Posicio (a,b)))))
  | a > x && b < y = alguPosicions t (init (tail (diagonalAB (Posicio (a,b)) (Posicio (x,y)))))
  | a > x && b > y = alguPosicions t (init (tail (diagonalBA (Posicio (x,y)) (Posicio (a,b)))))
 
  
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en horitzontal
   Precondicions: Les dues posicions es troben a la mateixa fila i la primera ha de ser més gran o igual que la segona, i han d'estar dins dels marges del tauler -}
   -- EX: horitzontal (Posicio ('g',3)) (Posicio ('d',3))
horitzontal :: Posicio -> Posicio -> [Posicio]
horitzontal (Posicio (a,b)) (Posicio (x,y))
 | (a,b) == (x,y) = [(Posicio (x,y))]
 | otherwise = [(Posicio (a,b))] ++ (horitzontal (Posicio (((!+) a (-1)),b)) (Posicio (x,y)))
 
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en vertical
   Precondicions: Les dues posicions es troben a la mateixa columna, la primera ha de ser més gran o igual que la segona, i han d'estar dins dels marges del tauler -}
   -- EX: vertical (Posicio ('g',8)) (Posicio ('g',2))
vertical :: Posicio -> Posicio -> [Posicio]
vertical (Posicio (a,b)) (Posicio (x,y))
 | (a,b) == (x,y) = [(Posicio (a,b))]
 | otherwise = [(Posicio (a,b))] ++ (vertical (Posicio (a,b-1)) (Posicio (x,y)))
 
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en la diagonal que comença per adalt a l'esquerra i acaba abaix a la dreta
   Precondicions: Les dues posicions es troben a la mateixa diagonal, la primera ha de ser la que estroba més cap a la dreta en el tauler, i han d'estar dins dels marges del tauler -}
   -- EX: diagonalAB (Posicio ('f',3)) (Posicio ('a',8))
diagonalAB :: Posicio -> Posicio -> [Posicio]
diagonalAB (Posicio (a,b)) (Posicio (x,y))
 | (a,b) == (x,y) = [(Posicio (a,b))]
 | otherwise = [(Posicio (a,b))] ++ (diagonalAB (Posicio (((!+)a (-1)),b+1)) (Posicio (x,y)))
 
{- Retorna totes les posicions que hi ha entre una i l'altra, amb elles incloses, en la diagonal que comença per abaix a l'esquerra i acaba adalt a la dreta
   Precondicions: Les dues posicions es troben a la mateixa diagonal, la primera ha de ser la que estroba més cap a la l'esquerra en el tauler, i han d'estar dins dels marges del tauler -}
   -- EX: diagonalBA (Posicio ('b',2)) (Posicio ('g',7))
diagonalBA :: Posicio -> Posicio -> [Posicio]
diagonalBA (Posicio (a,b)) (Posicio (x,y))
 | (a,b) == (x,y) = [(Posicio (a,b))]
 | otherwise = [(Posicio (a,b))] ++ (diagonalBA (Posicio (((!+)a 1),b+1)) (Posicio (x,y)))
 
{- Retorna cert si algunes de les posicions donades té alguna Peça diferent de B, és a dir, si no és buida, altrament fals
   Precondició: Les posicions entrades tenen que ser legals (dins del tauler) -}
alguPosicions :: Tauler -> [Posicio] -> Bool
alguPosicions (Tauler t) [y] = if ((posicioBuida (Tauler t) y) == False) then True else False
alguPosicions (Tauler t) (y:ys) = if ((posicioBuida (Tauler t) y) == False) then True else (alguPosicions (Tauler t) ys)

{- Retorna Cert si la Posicio entrada no es troba al Tauler entrat, és a dir, si no hi ha cap peça en aquella posició, altrament retorna fals. -}
posicioBuida :: Tauler -> Posicio -> Bool
posicioBuida (Tauler [x]) p = if ((snd x) == p) then False else True
posicioBuida (Tauler (x:xs)) p = if ((snd x) == p) then False else (posicioBuida (Tauler xs) p)



--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

-------------------AUXILIARS----------------------
-------------------FesJugada----------------------
--------------------------------------------------
menorPosicio :: Posicio -> Posicio -> Bool
menorPosicio (Posicio(a,b)) (Posicio(c,d))
 | b > d = True
 | b == d && a < c = True
 | otherwise = False
 

inserirOrdenat :: (Peca, Posicio) -> [(Peca, Posicio)] -> [(Peca, Posicio)]
inserirOrdenat item [] = [item]
inserirOrdenat item (first:ls)
 | (snd item) == (snd first) = item : ls            --Son el mateix
 | menorPosicio (snd item) (snd first) = item : first : ls
 -- | (snd item) > (snd first) = item : first : ls     --No existeix, originalment: <
 | otherwise = first : (inserirOrdenat item ls)

eliminarPosicio :: Posicio -> [(Peca, Posicio)] -> [(Peca, Posicio)]
eliminarPosicio pos [] = [] -- no s'ha trobat
eliminarPosicio pos (first:ls)
 | pos == (snd first) = ls                   --Son el mateix
 | otherwise = first : (eliminarPosicio pos ls)
-- contePosicio :: Posicio -> Tauler -> Bool
-- contePosicio 

{- 
pre: La jugada es legal
La funció fesJugada retorna el Tauler inicial però amb la jugada efectuada. 
Y es origen, Z es desti
-}
fesJugada :: Tauler -> Jugada -> Tauler
fesJugada (Tauler t) (x,y,z) = Tauler(inserirOrdenat (x,z) (eliminarPosicio y t))

--------------------------------------------------
--------------------------------------------------
--------------------------------------------------

  
{- La funció moviment ... saber donada una Peca i una posició, quines serien totes les posicions on podria anar en un tauler buit. -}
moviment :: Peca -> Posicio -> [Posicio]
moviment x y
  | (x == PN) || (x == PB) = movimentPeo y (colorPeca x)
  | (x == CN) || (x == CB) = movimentCavall y
  | (x == AN) || (x == AB) = movimentAlfil y
  | (x == TN) || (x == TB) = movimentTorre y
  | (x == DN) || (x == DB) = (movimentTorre y)++(movimentAlfil y)
  | (x == RN) || (x == RB) = movimentRei y
  | otherwise = error "No existeix aquesta Peca"


--------------------------------------- INICI FUNCIÓ ESCAC --------------------------------------
-------------------------------------------------------------------------------------------------

{- La funció escac retorna cert si al tauler entrat el rei del color entrat té alguna peça rival que li pot fer escac, altrament fals -}
escac :: Tauler -> Color -> Bool
escac (Tauler t) c = taulerEscac (Tauler t) c (posAct (Tauler t) c 'R')

{- Retorna cert si alguna peca contraria al color donat pot matar al rei del contrari, que es troba a la posicio donada, altrament fals -}
taulerEscac :: Tauler -> Color -> Posicio -> Bool
taulerEscac (Tauler [x]) c p = if (esRival (fst x) c) then (if (faEscac x p) then True else False) else False
taulerEscac (Tauler (x:xs)) c p = if (esRival (fst x) c) then (if (faEscac x p) then True else taulerEscac (Tauler xs) c p) else taulerEscac (Tauler xs) c p

{- Retorna la posicio de la peça introduida com a caracter al tauler entrat, amb el color entrat -}
   -- EX: posAct taulerInicial Negre 'R'
posAct :: Tauler -> Color -> Char -> Posicio
posAct (Tauler t) c o = if ((elemIndex (pecaResultant c o) (map fst t))==Nothing) then (error "No existeix al tauler") else snd (t!!(obtEnter (elemIndex (pecaResultant c o) (map fst t))))

{- Retorna l'enter del maybe int entre 0 i 31, altrament retorna 32, que està fora del màxim de peces possibles al tauler -}
obtEnter :: Maybe Int -> Int
obtEnter (Just x)
 | x>(-1) && x<32 = x
 | otherwise = 32

{- Retorna la Peca en funció del color entrar i el caracter -}
pecaResultant :: Color -> Char -> Peca
pecaResultant Blanc x = if (x=='P') then PB else (if (x=='C') then CB else (if (x=='A') then AB else (if (x=='T') then TB else (if (x=='D') then DB else RB))))
pecaResultant Negre x = if (x=='P') then PN else (if (x=='C') then CN else (if (x=='A') then AN else (if (x=='T') then TN else (if (x=='D') then DN else RN))))

{- Retorna cert si la peca entrada no pertany al color entrat, és a dir, si són rivals -}
esRival :: Peca -> Color -> Bool
esRival p c = if (((p==PN || p==CN || p==AN || p==TN || p==DN || p==RN) && (c == Blanc)) || ((p==PB || p==CB || p==AB || p==TB|| p==DB|| p==RB) && (c == Negre))) then True else False

{- Retorna cert si la peca en la posició entrada té algun moviment que la porti a la segona posicio, altrament retorna fals -}
   -- EX: faEscac (TN, (Posicio ('a',7))) (Posicio ('a',1)), faEscac (AN, (Posicio ('c',8))) (Posicio ('h',3))
faEscac :: (Peca,Posicio) -> Posicio -> Bool
faEscac (x,y) p = if ((find (==p) (moviment x y)) == Nothing) then False else True

---------------------------------------- FI FUNCIÓ ESCAC ----------------------------------------
-------------------------------------------------------------------------------------------------


 
----------------------------MAIN------------------------------
-- errorException :: IOError -> IO a
-- errorException e
--  | isDoesNotExistError e = print("ERROR: No existeix el fitxer")
--  | errorDesconegut e = error("ERROR: desconegut")

-- main = do 
--  putStrLn "Benvingut a l'analitzador d'Escacs"
--  putStrLn "Introdueix el nom del fitxer:"
--  nomFitxer <- getLine
--  fitxer <- try $ readFile nomFitxer 
--  putStrLn fitxer
--  -- analitzarPartida lines fitxer